﻿using CheckersClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Checkers
{
    public partial class EndOfGameWindow : Window
    {
        public EndOfGameWindow(WinnerEnum winner)
        {
            InitializeComponent();
            DefineContent(winner);
        }

        private void DefineContent(WinnerEnum winner)
        {
            if (winner == WinnerEnum.Black)
            {
                WinnerBorder.Background = Brushes.Black;
                WinnerTextBlock.Text = "Black Won!";
                WinnerTextBlock.Foreground = Brushes.White; 
            }
            else if (winner == WinnerEnum.White)
            {
                WinnerBorder.Background = Brushes.White;
                WinnerTextBlock.Text = "White Won!";
                WinnerTextBlock.Foreground = Brushes.Black;
            }
            else if (winner == WinnerEnum.Draw)
            {
                WinnerBorder.Background = Brushes.DarkGray;
                WinnerTextBlock.Text = "Draw";
                WinnerTextBlock.Foreground = Brushes.White;
            }
        }

        private void StartNewGameButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
            ((GameWindow)Owner).StartNewGame();
        }

        private void SaveGameAndStartNewButton_Click(object sender, RoutedEventArgs e)
        {
            ((GameWindow)Owner).SaveGameAsync();
            StartNewGameButton_Click(sender, e);
        }

        private void GoToMenuButton_Click(object sender, RoutedEventArgs e)
        {
            MenuWindow window = new MenuWindow();
            window.Show();
            Owner.Close();
            Close();
        }
    }
}
