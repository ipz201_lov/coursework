﻿using CheckersClassLibrary;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Checkers
{
    public partial class GameOverlookWindow : Window
    {
        private Dictionary<int, Board> boardHistoryDictionary;
        private Board chosenBoard;
        private int chosenBoardIndex;
        private ToggleButton[,] buttons;

        public GameOverlookWindow(GameRecording gr)
        {
            InitializeComponent();
            boardHistoryDictionary = new Dictionary<int, Board>();
            List<Board> boardHistory = JsonConvert.DeserializeObject<List<Board>>(gr.boardHistory);
            boardHistory.Reverse();
            foreach (var board in boardHistory.ToList().Select((o, index) => new { o, index })) {
                boardHistoryDictionary[board.index] = board.o;
            }
            chosenBoard = boardHistory[boardHistory.Count - 1];
            chosenBoardIndex = boardHistory.Count - 1;
            AddButtonsOnTheGrid();
            WinnerTableBorder.Tag = WinnerTable.Tag = (gr.winner == "Black") ? "Black" : (gr.winner == "White") ? "White" : "Draw"; 
        }

        public void AddButtonsOnTheGrid()
        {
            buttons = new ToggleButton[8, 8];
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    buttons[i, j] = new ToggleButton();
                    buttons[i, j].Background = ((i + j) % 2 == 0) ? Brushes.LightGreen : Brushes.Gray;
                    buttons[i, j].SetResourceReference(StyleProperty, "FieldImage");
                    SetButtonTag(i,j);
                    Grid.SetRow(buttons[i, j], i);
                    Grid.SetColumn(buttons[i, j], j);
                    FieldsOverlook.Children.Add(buttons[i, j]);
                }
            }
        }

        public void SetButtonTag(int i, int j)
        {
            if (chosenBoard[i, j] == null)
            {
                buttons[i, j].Tag = "";
            }
            else if (chosenBoard[i, j].IsWhite == true && chosenBoard[i, j].IsKing == false)
            {
                buttons[i, j].Tag = "WhiteChecker";
            }
            else if (chosenBoard[i, j].IsWhite == false && chosenBoard[i, j].IsKing == false)
            {
                buttons[i, j].Tag = "BlackChecker";
            }
            else if (chosenBoard[i, j].IsWhite == false && chosenBoard[i, j].IsKing == true)
            {
                buttons[i, j].Tag = "BlackKing";
            }
            else if (chosenBoard[i, j].IsWhite == true && chosenBoard[i, j].IsKing == true)
            {
                buttons[i, j].Tag = "WhiteKing";
            }
        }

        private void TurnBackButton_Click(object sender, RoutedEventArgs e)
        {
            if(chosenBoardIndex - 1 >= 0)
            {
                chosenBoard = boardHistoryDictionary[--chosenBoardIndex];
                AddButtonsOnTheGrid();
            }  
        }

        private void NextMoveButton_Click(object sender, RoutedEventArgs e)
        {
            if (chosenBoardIndex + 1 < boardHistoryDictionary.Count)
            {
                chosenBoard = boardHistoryDictionary[++chosenBoardIndex];
                AddButtonsOnTheGrid();
            }
        }

        private void GamesDBButton_Click(object sender, RoutedEventArgs e)
        {
            GamesDBWindow window = new GamesDBWindow();
            window.Show();
            Close();
        }

        private void MainMenuButton_Click(object sender, RoutedEventArgs e)
        {
            MenuWindow window = new MenuWindow();
            window.Show();
            Close();
        }

        private void RotateBoardButton_Click(object sender, RoutedEventArgs e)
        {
            if (((Button)sender).Tag.ToString() == "0")
            {
                FieldsOverlook.RenderTransform =
                    new RotateTransform(180, FieldsOverlook.ActualWidth / 2.0, FieldsOverlook.ActualHeight / 2.0);
                ((Button)sender).Tag = "1";
            }
            else
            {
                FieldsOverlook.RenderTransform =
                   new RotateTransform(0, FieldsOverlook.ActualWidth / 2.0, FieldsOverlook.ActualHeight / 2.0);
                ((Button)sender).Tag = "0";
            }
        }
    }
}
