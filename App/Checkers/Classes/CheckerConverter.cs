﻿using CheckersClassLibrary;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Data;

namespace Checkers
{
    public class CheckerConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Checker checker = (Checker)value;
            if (checker == Board.WhiteChecker)
                return "WhiteChecker";
            if (checker == Board.BlackChecker)
                return "BlackChecker";
            if (checker == Board.WhiteKing)
                return "WhiteKing";
            if (checker == Board.BlackKing)
                return "BlackKing";
            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
