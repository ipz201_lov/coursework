﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Checkers
{
    class AppContext : DbContext
    {
        public DbSet<GameRecording> GameRecordings { get; set; }

        public AppContext() : base("DefaultConnection") {
            Database.SetInitializer<AppContext>(null);
        }
    }
}
