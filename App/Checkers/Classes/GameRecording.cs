﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CheckersClassLibrary;
using Newtonsoft.Json;

namespace Checkers
{
    [Table("GameRecordings")]
    public class GameRecording
    {
        [Key]
        public int id { get; set; }
        public string date { get; set; }
        public string winner { get; set; }
        public string boardHistory { get; set; }

        public GameRecording() { }

        public GameRecording(string date, string winner, Stack<Board> boardHistory) {
            this.date = date;
            this.winner = winner;
            this.boardHistory = JsonConvert.SerializeObject(boardHistory);
        }
    }
}
