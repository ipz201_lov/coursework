﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Checkers
{
    public partial class GamesDBWindow : Window
    {
        AppContext _context;
        public GamesDBWindow()
        {
            InitializeComponent();
            _context = new AppContext();
            List<GameRecording> list = _context.GameRecordings.ToList();
            list.Reverse();
            SavedGames.ItemsSource = list;
        }

        private void MainMenuButton_Click(object sender, RoutedEventArgs e)
        {
            MenuWindow window = new MenuWindow();
            window.Show();
            Close();
        }

        private void LookButton_Click(object sender, RoutedEventArgs e)
        {
            GameRecording ctx = (GameRecording)((Button)sender).DataContext;
            GameOverlookWindow window = new GameOverlookWindow(ctx);
            window.Show();
            Close();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            GameRecording ctx = (GameRecording)((Button)sender).DataContext;
            DeleteGameRecordingAsync(ctx);
        }

        private async void DeleteGameRecordingAsync(GameRecording ctx) {
            await Task.Run(() => DeleteGameRecording(ctx));
            GetAllRecordings();
        }

        private void DeleteGameRecording(GameRecording ctx)
        {
            if (_context.GameRecordings.Find(ctx.id) != null)
            {
                _context.GameRecordings.Remove(ctx);
                _context.SaveChanges();
            }
        }

        private void GetAllRecordings() {
            List<GameRecording> list = _context.GameRecordings.ToList();
            list.Reverse();
            SavedGames.ItemsSource = list;
        }
    }
}
