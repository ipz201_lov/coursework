﻿using CheckersClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Checkers
{
    public partial class OfferADrawWindow : Window
    {
        public OfferADrawWindow()
        {
            InitializeComponent();
        }

        private void AcceptButton_Click(object sender, RoutedEventArgs e)
        {
            GameWindow gameWindow = ((GameWindow)Owner);
            gameWindow.currentGame.Winner = WinnerEnum.Draw;
            gameWindow.OpenWinnerWindow();
            Close();
        }

        private void DeclineButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
