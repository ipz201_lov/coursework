﻿using CheckersClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Checkers
{
    public partial class GiveUpConfirmationWindow : Window
    {
        public GiveUpConfirmationWindow()
        {
            InitializeComponent();
        }

        private void SurrenderButton_Click(object sender, RoutedEventArgs e)
        {
            GameWindow gameWindow = ((GameWindow)Owner);
            gameWindow.currentGame.Winner = (gameWindow.currentGame.IsWhitesTurn) ? WinnerEnum.Black : WinnerEnum.White;
            gameWindow.OpenWinnerWindow();
            Close();
        }

        private void NotSurrenderButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
