﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CheckersClassLibrary;

namespace Checkers
{
    public partial class GameWindow : Window
    {
        AppContext _context;
        public Game currentGame;
        private ToggleButton[,] buttons;
        public GameWindow()
        {
            InitializeComponent();
            _context = new AppContext();
            StartNewGame();
        }

        public void StartNewGame()
        {
            InitializeGame();
            AddButtonsOnTheGrid();
            AddEventListenersForTheButtons();
        }

        private void InitializeGame()
        {
            currentGame = new Game();
        }

        private void AddButtonsOnTheGrid()
        {
            buttons = new ToggleButton[8, 8];
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    buttons[i, j] = new ToggleButton();
                    buttons[i, j].Background = ((i + j) % 2 == 0) ? Brushes.LightGreen : Brushes.Gray;
                    buttons[i, j].SetResourceReference(StyleProperty, "FieldImage");
                    ConfigureBinding(i, j);
                    Grid.SetRow(buttons[i, j], i);
                    Grid.SetColumn(buttons[i, j], j);
                    Fields.Children.Add(buttons[i, j]);
                }
            }
        }

        private void ConfigureBinding(int i, int j) 
        {
            buttons[i,j].SetBinding(TagProperty,
                        CreateBinding(currentGame, $"Board[{i},{j}]", new CheckerConverter()));

            Binding CreateBinding(object Source, string path, IValueConverter converter)
            {
                Binding binding = new Binding(path);
                binding.Source = Source;
                binding.Converter = converter;
                return binding;
            }
        }

        private void UpdateAllBindings() 
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    ConfigureBinding(i, j);
                }
            }
        }

        private void AddEventListenersForTheButtons()
        {
            for (int i = 0; i < 8; i++) 
            {
                for (int j = 0; j < 8; j++)
                {
                    int iParam = i; 
                    int jParam = j; 
                    buttons[i,j].Click +=
                        (sender, e) => 
                        {
                            if (((ToggleButton)sender).IsChecked == true)
                            {
                                Button_Pressed(iParam, jParam, e);
                            }
                            else
                            {
                                Button_Unpressed(iParam, jParam, e);
                            }
                        };
                }
            }
        }

        private void Button_Pressed(int i, int j, RoutedEventArgs e)
        {
            UncheckAllButtons();
            currentGame.SetChosenChecker(i, j);
            HighlightAvailableFields();
        }

        private void Button_Unpressed(int i, int j, RoutedEventArgs e)
        {
            currentGame.MakeAMove(currentGame.ChosenChecker.i, currentGame.ChosenChecker.j, i, j);
            UpdateAllBindings();
            UncheckAllButtons();
            Button_Pressed(i, j, e);

            if (currentGame.Winner != WinnerEnum.NoWinner) {
                OpenWinnerWindow();
            }
        }

        public void OpenWinnerWindow()
        {
            EndOfGameWindow endOfGameWindow = new EndOfGameWindow(currentGame.Winner) { Owner = this };
            endOfGameWindow.ShowDialog();
        }

        private void HighlightAvailableFields() 
        {
            foreach (var checker in currentGame.AvailableFieldsForChosen)
            {
                buttons[checker.i, checker.j].IsChecked = true;
            }
        }

        private void UncheckAllButtons()
        {
            foreach (var button in buttons)
            {
                button.IsChecked = false;
            }
        }

        private void RotateBoardButton_Click(object sender, RoutedEventArgs e)
        {
            if (((Button)sender).Tag.ToString() == "0")
            {
                Fields.RenderTransform =
                    new RotateTransform(180, Fields.ActualWidth / 2.0, Fields.ActualHeight / 2.0);
                ((Button)sender).Tag = "1";
            }
            else
            {
                Fields.RenderTransform =
                   new RotateTransform(0, Fields.ActualWidth / 2.0, Fields.ActualHeight / 2.0);
                ((Button)sender).Tag = "0";
            }
        }

        private void TurnBackButton_Click(object sender, RoutedEventArgs e)
        {
            currentGame.TurnBack();
            UpdateAllBindings();
        }

        private void MainMenuButton_Click(object sender, RoutedEventArgs e)
        {
            MenuWindow window = new MenuWindow();
            window.Show();
            Close();
        }

        private void OfferADrawButton_Click(object sender, RoutedEventArgs e)
        {
            OfferADrawWindow window = new OfferADrawWindow() { Owner = this };
            window.ShowDialog();
        }

        private void GiveUpButton_Click(object sender, RoutedEventArgs e)
        {
            GiveUpConfirmationWindow window = new GiveUpConfirmationWindow() { Owner = this };
            window.ShowDialog();
        }

        public async void SaveGameAsync()
        {
            await Task.Run(() => SaveGame());
        }

        private void SaveGame() 
        {
            string winner = (currentGame.Winner == WinnerEnum.Black) ? 
                            "Black" : (currentGame.Winner == WinnerEnum.White) ? 
                            "White" : "Draw";
            GameRecording gr = new GameRecording(DateTime.Now.ToString(), winner, currentGame.BoardHistory);
            _context.GameRecordings.Add(gr);
            _context.SaveChanges();
        }

        private void GamesDBButton_Click(object sender, RoutedEventArgs e)
        {
            GamesDBWindow window = new GamesDBWindow();
            window.Show();
            Close();
        }
    }
}
