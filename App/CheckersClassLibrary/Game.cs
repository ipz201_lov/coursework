﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheckersClassLibrary
{
    public class Game
    {
        public bool IsWhitesTurn { private set; get; }
        public WinnerEnum Winner { get; set; }

        public (int i, int j) ChosenChecker { get; private set; }
        public List<(int i, int j)> AvailableFieldsForChosen { get; private set; }

        public List<(int i, int j)> CheckersThatCanEat { get; private set; }
        public List<(int i, int j)> EatenCheckers { get; private set; }
        public Board Board { get; private set; }
        public Stack<Board> BoardHistory { get; private set; }

        public Game()
        {
            Board = new Board();
            IsWhitesTurn = true;
            CheckersThatCanEat = new List<(int i, int j)>();
            EatenCheckers = new List<(int i, int j)>();
            AvailableFieldsForChosen = new List<(int i, int j)>();
            BoardHistory = new Stack<Board>();
            BoardHistory.Push(Board.Copy());
            Winner = WinnerEnum.NoWinner;
        }

        public Checker this[int i, int j]
        {
            get
            {
                return Board[i, j];
            }
        }

        public void ChangeTurn()
        {
            IsWhitesTurn = !IsWhitesTurn;
            BoardHistory.Push(Board.Copy());
            IsThereWinner();
            FindCheckersThatCanEat();
        }

        public void TurnBack()
        {
            if (BoardHistory.Count > 1 && EatenCheckers.Count == 0)
            {
                IsWhitesTurn = !IsWhitesTurn;
                Winner = WinnerEnum.NoWinner;
                BoardHistory.Pop();
                Board = BoardHistory.Peek().Copy();
                FindCheckersThatCanEat();
            }
        }

        public void SetChosenChecker(int i, int j)
        {
            if (IsOnTheBoard(i, j))
            {
                ChosenChecker = (i, j);
                CheckAvailableFields(i, j);
            }
        }

        private void FindCheckersThatCanEat()
        {
            CheckersThatCanEat.Clear();
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (Board[i, j] != null && Board[i, j].IsWhite == IsWhitesTurn)
                    {
                        FindOutHowCheckerCanEat(i, j);
                    }
                }
            }
        }

        private bool CheckAvailableFields(int i, int j)
        {
            AvailableFieldsForChosen.Clear();

            if (Board[i, j] != null && Board[i, j].IsWhite == IsWhitesTurn)
            {
                if (CheckersThatCanEat.Count == 0)
                {
                    return FindOutWhereCheckerCanGo(i, j);
                }
                else if (CheckersThatCanEat.Contains((i, j)))
                {
                    return FindOutHowCheckerCanEat(i, j);
                }
            }

            return false;
        }

        public bool FindOutWhereCheckerCanGo(int i, int j)
        {
            if (!IsOnTheBoard(i, j) && Board[i, j] != null)
            {
                return false;
            }

            if (Board[i, j].IsKing == false)
            {
                if (Board[i, j].IsWhite == true)
                {
                    if (IsOnTheBoard(i - 1, j - 1) && Board[i - 1, j - 1] == null)
                        AvailableFieldsForChosen.Add((i - 1, j - 1));

                    if (IsOnTheBoard(i - 1, j + 1) && Board[i - 1, j + 1] == null)
                        AvailableFieldsForChosen.Add((i - 1, j + 1));
                }
                else
                {
                    if (IsOnTheBoard(i + 1, j + 1) && Board[i + 1, j + 1] == null)
                        AvailableFieldsForChosen.Add((i + 1, j + 1));

                    if (IsOnTheBoard(i + 1, j - 1) && Board[i + 1, j - 1] == null)
                        AvailableFieldsForChosen.Add((i + 1, j - 1));
                }
            }
            else
            {
                for (int i1 = -1; i1 <= 1; i1 += 2)
                {
                    for (int j1 = -1; j1 <= 1; j1 += 2)
                    {
                        for (int k = 1; k <= 7; k++)
                        {
                            if (IsOnTheBoard(i + i1 * k, j + j1 * k) && Board[i + i1 * k, j + j1 * k] != null)
                                break;

                            if (IsOnTheBoard(i + i1 * k, j + j1 * k) && Board[i + i1 * k, j + j1 * k] == null)
                                AvailableFieldsForChosen.Add((i + i1 * k, j + j1 * k));
                        }
                    }
                }
            }

            if (AvailableFieldsForChosen.Count == 0)
                return false;

            return true;
        }

        public bool FindOutHowCheckerCanEat(int i, int j)
        {
            if (!IsOnTheBoard(i, j) && Board[i, j] != null)
            {
                return false;
            }

            bool flag = false;

            for (int directionI = -1; directionI <= 1; directionI += 2)
            {
                for (int directionJ = -1; directionJ <= 1; directionJ += 2)
                {
                    for (int k = 1; k <= 6; k++)
                    {
                        if (Board[i, j].IsKing == false && k > 1)
                            break;

                        if (IsOnTheBoard(i + directionI * k, j + directionJ * k) &&
                            Board[i + directionI * k, j + directionJ * k] != null)
                        {
                            if (EatenCheckers.Contains((i + directionI * k, j + directionJ * k)))
                                break;

                            if (Board[i + directionI * k, j + directionJ * k].IsWhite == Board[i, j].IsWhite)
                                break;

                            for (int z = 1; z <= 6; z++)
                            {
                                if (Board[i, j].IsKing == false && z > 1)
                                    break;

                                if (IsOnTheBoard(i + directionI * (k + z), j + directionJ * (k + z)) &&
                                    Board[i + directionI * (k + z), j + directionJ * (k + z)] != null)
                                    break;

                                if (IsOnTheBoard(i + directionI * (k + z), j + directionJ * (k + z)) && 
                                    Board[i + directionI * (k + z), j + directionJ * (k + z)] == null)
                                {
                                    AvailableFieldsForChosen.Add(
                                        (i + directionI * (k + z), j + directionJ * (k + z))
                                    );

                                    flag = true;
                                }
                            }

                            break;
                        }
                    }
                }
            }

            if (flag == true)
            {
                CheckersThatCanEat.Add((i, j));
                return true;
            }

            return false;
        }

        private bool IsOnTheBoard(int i, int j)
        {
            if (i >= 0 && i <= 7 && j >= 0 && j <= 7)
                return true;
            return false;
        }

        public void MakeAMove(int startI, int startJ, int destI, int destJ)
        {
            if (Math.Abs(startI - destI) == Math.Abs(startJ - destJ))
            {
                Checker temp = Board[startI, startJ];
                Board[startI, startJ] = Board[destI, destJ];
                Board[destI, destJ] = temp;
                
                for (int i = startI, j = startJ; 
                        (i == destI && j == destJ) ? false : true;
                        i = (startI < destI) ? ++i : --i,
                        j = (startJ < destJ) ? ++j : --j) 
                {
                    if ((i,j) != (destI,destJ) && Board[i, j] != Board.EmptyField)
                    {
                        EatenCheckers.Add((i, j));
                    }
                }

                if (Board[destI, destJ] != null && Board[destI, destJ].IsWhite == true && destI == 0)
                    Board[destI, destJ] = Board.WhiteKing;

                if (Board[destI, destJ] != null && Board[destI, destJ].IsWhite == false && destI == 7)
                    Board[destI, destJ] = Board.BlackKing;

                CheckersThatCanEat.Clear();
                if (EatenCheckers.Count > 0 && FindOutHowCheckerCanEat(destI, destJ))
                {
                    CheckersThatCanEat.Add((destI, destJ));
                }
                else
                {
                    RemoveAllBeatenCheckers();
                    ChangeTurn();
                }
            }
        }

        public void RemoveAllBeatenCheckers() {
            foreach (var checker in EatenCheckers) 
            {
                Board[checker.i, checker.j] = Board.EmptyField;
            }
            EatenCheckers.Clear();
        }

        public bool IsThereWinner()
        {
            FindCheckersThatCanEat();
            for (int i = 0; i < 8; i++) 
            {
                for (int j = 0; j < 8; j++)
                {
                    if (CheckAvailableFields(i, j)) 
                    {
                        return false;
                    }
                }
            }

            if (IsWhitesTurn == true) Winner = WinnerEnum.Black;
            if (IsWhitesTurn == false) Winner = WinnerEnum.White;
            return true;
        }
    }
}
