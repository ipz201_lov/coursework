﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheckersClassLibrary
{
    public enum WinnerEnum
    {
        White,
        Black,
        Draw,
        NoWinner
    }
}
