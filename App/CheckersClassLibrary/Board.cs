﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheckersClassLibrary
{
    public class Board
    {
        private Checker[,] allCheckers;
        public static readonly Checker WhiteChecker = new Checker(true, false);
        public static readonly Checker BlackChecker = new Checker(false, false);
        public static readonly Checker WhiteKing = new Checker(true, true);
        public static readonly Checker BlackKing = new Checker(false, true);
        public static readonly Checker EmptyField = null;

        public Board()
        {
            InitializeBoard();
        }

        public Checker[,] AllCheckers
        {
            get
            {
                return allCheckers;
            }
            set
            {
                allCheckers = value;
            }

        }

        public Checker this[int i, int j]
        {
            get
            {
                return allCheckers[i, j];
            }
            set
            {
                allCheckers[i, j] = value;
            }
        }

        public void InitializeBoard()
        {
            allCheckers = new Checker[,] {
                { EmptyField, BlackChecker, EmptyField, BlackChecker, EmptyField, BlackChecker, EmptyField, BlackChecker },
                { BlackChecker, EmptyField, BlackChecker, EmptyField, BlackChecker, EmptyField, BlackChecker, EmptyField },
                { EmptyField, BlackChecker, EmptyField, BlackChecker, EmptyField, BlackChecker, EmptyField, BlackChecker },
                { EmptyField, EmptyField, EmptyField, EmptyField, EmptyField, EmptyField, EmptyField, EmptyField },
                { EmptyField, EmptyField, EmptyField, EmptyField, EmptyField, EmptyField, EmptyField, EmptyField },
                { WhiteChecker, EmptyField, WhiteChecker, EmptyField, WhiteChecker, EmptyField, WhiteChecker, EmptyField },
                { EmptyField, WhiteChecker, EmptyField, WhiteChecker, EmptyField, WhiteChecker, EmptyField, WhiteChecker },
                { WhiteChecker, EmptyField, WhiteChecker, EmptyField, WhiteChecker, EmptyField, WhiteChecker, EmptyField }
            };
        }

        public Board Copy()
        {
            Board board = new Board();
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++)
                {
                    board[i, j] = allCheckers[i, j];
                }
            }
            return board;
        }
    }
}
