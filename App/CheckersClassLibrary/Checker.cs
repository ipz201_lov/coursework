﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheckersClassLibrary
{
    public class Checker
    {
        public bool IsWhite { get; set; }
        public bool IsKing { get; set; }

        public Checker(bool isWhite, bool isKing)
        {
            IsWhite = isWhite;
            IsKing = isKing;
        }
    }
}
